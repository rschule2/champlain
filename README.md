-------------------------------------------------
Courses Developed and Taught at Champlain College
-------------------------------------------------

In 2014, a former colleague at the Computer Science department for Champlain College, Burlington, VT, asked me to teach a course.  As a result, I developed and taught the courses described below.  This repository includes course descriptions, and sample hardware and software projects.

-- Ray Schuler

----------------------------------------------
CSI 280 51: Open Source Software Development
----------------------------------------------

CSI 280 provides junior/senior CS majors a real-world experience in collaborative software development, utilizing state of the art tools.  The FOSS model provides an excellent platform for experimentation and collaboration within the typical university infrastructure.

For student needing a jump start, I open sourced a personal project, a Raspberry Pi based thermometer that I created and use in my home.  This was a popular 'challenge' and for many students an introduction to writing code to control hardware.

https://bitbucket.org/rschule2/champlain/src/master/csi-280-51-schuler-syllabus-spring-2014-20131221.pdf

https://bitbucket.org/rschule2/champlain/src/master/csi-280-raspberry-pi-thermometer.pdf

https://bitbucket.org/rschule2/champlain/src/master/csi-280-raspberry-pi-challenge.pdf


-------------------------------------
CSI 230 51/52: Linux/Unix Programming
-------------------------------------

CSI 230 extends student knowledge C/C++/Java in a MS Windows environment, to the Linux/Unix platform.  Students moved from basic shell scripting, to a thorough understanding of the Linux ABI, Standard Library calls, System calls, as well as the Linux kernel.

https://bitbucket.org/rschule2/champlain/src/master/csi-230-51-52-schuler-syllabus-fall-2014-20140819.pdf


-----------
Downloading
-----------

Clone this repository to your system with:
git clone https://bitbucket.org/rschule2/champlain.git

Or use the interface at 'https://bitbucket.org/rschule2/champlain' to download specific files.  Your system may require you to choose 'view raw' to download.

